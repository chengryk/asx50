package cheng.ray.asx50.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class StreamUtils {
    private static final int STREAM_BUFFER_LENGTH = 1024;
    public static String streamToString(InputStream in) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[STREAM_BUFFER_LENGTH];
        int length;
        while ((length = in.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString(StandardCharsets.UTF_8.name());
    }
    public static void closeInputStream(InputStream stream){
        try {
            if (stream != null) {
                stream.close();
            }
        } catch (IOException ignored){}
    }
    public static InputStream stringToStream(String source) throws UnsupportedEncodingException {
        return new ByteArrayInputStream(source.getBytes( StandardCharsets.UTF_8.name()));
    }
}
