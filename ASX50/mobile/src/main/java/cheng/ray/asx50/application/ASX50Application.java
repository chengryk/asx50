package cheng.ray.asx50.application;

import android.app.Application;

import cheng.ray.asx50.persistence.StockPriceDbHelper;

public class ASX50Application extends Application {
    private final StockPriceDbHelper stockPriceDbHelper = new StockPriceDbHelper(this);

    public ASX50Application() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public StockPriceDbHelper getStockPriceDbHelper(){return stockPriceDbHelper;}
}
