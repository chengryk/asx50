package cheng.ray.asx50.model;

import android.text.TextUtils;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

import cheng.ray.asx50.util.StreamUtils;

public class ASX50StockParser extends ASX50Parser {
    private static final String PRICE_TAG = "sectionQuoteDetail";
    private static final String START_PRICE_TAG = "<div";
    private static final String END_PRICE_TAG = "</div>";
    private static final String START_OVERALL_TAG = "<div id=\"overallRatios\"";
    private static final String START_FINANCIALS_TAG = "<div id=\"companyVsIndustry\"";
    private static final String START_TBODY_TAG = "<tbody>";
    private static final String END_TBODY_TAG = "</tbody>";

    private String code = null;

    public void setCode(String code){this.code = code;}

    private InputStream trimPriceData(String html){
        InputStream output = null;
        try {
            if (!TextUtils.isEmpty(html)){
                int start = html.indexOf(PRICE_TAG);
                start = html.lastIndexOf(START_PRICE_TAG, start);

                int end = html.indexOf(END_PRICE_TAG, start);
                String trimmed = TextUtils.substring(html, start, end + END_PRICE_TAG.length()).replace("\n", "").replace("&nbsp;","");
                output = StreamUtils.stringToStream(trimmed);
            }
        } catch (IOException | IndexOutOfBoundsException ignored){}
        return output;
    }

    private InputStream trimModuleData(String html, String startTag){
        InputStream output = null;
        try {
            if (!TextUtils.isEmpty(html)){
                int start = html.indexOf(startTag);
                start = html.indexOf(START_TBODY_TAG, start);
                int end = html.indexOf(END_TBODY_TAG, start);
                String trimmed = TextUtils.substring(html, start, end + END_TBODY_TAG.length()).replace("\n", "").replace("&nbsp;","");
                output = StreamUtils.stringToStream(trimmed);
            }
        } catch (IOException | IndexOutOfBoundsException ignored){}
        return output;
    }

    private void readModuleData(XmlPullParser parser, Stock output) throws XmlPullParserException, IOException {
        // parser.nextTag();
        String currentAttribute;
        String currentValue;
        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            // Starts by looking for the entry tag
            if ("tr".equals(parser.getName())){
                parser.nextTag(); // @td
                if ("td".equals(parser.getName())) {
                    currentAttribute = readText(parser);
                    parser.nextTag(); // @td
                    if (!TextUtils.isEmpty(currentAttribute) &&
                            (currentAttribute.contains("Beta") || currentAttribute.contains("Yield"))
                            ) {
                        parser.nextTag(); // @strong
                    }
                    currentValue = readText(parser);

                    if (!TextUtils.isEmpty(currentAttribute)) {
                        if (currentAttribute.contains("Beta")) {
                            output.setBeta(currentValue);
                        } else if (currentAttribute.contains("Yield")) {
                            output.setYield(currentValue);
                        } else if (currentAttribute.contains("P/E (TTM)")) {
                            output.setPeRatio(currentValue);
                        }
                    }
                }
            }
        }
    }

    @Override
    public Stock parse(String html) throws XmlPullParserException, IOException {
            Stock output = new Stock(code);
            if (!TextUtils.isEmpty(html)) {
                parsePrice(html, output);
                parseModule(html, START_OVERALL_TAG, output);
                parseModule(html, START_FINANCIALS_TAG, output);
            }
            return output;
    }

    private void parseModule(String html, String startTag, Stock stock) throws XmlPullParserException, IOException {
        InputStream stream = null;
        try {
            stream = trimModuleData(html, startTag);
            XmlPullParser parser = getParser(stream);
            readModuleData(parser, stock);
        } finally {
            StreamUtils.closeInputStream(stream);
        }
    }


    private void parsePrice(String html, Stock stock) throws XmlPullParserException, IOException {
        InputStream stream = null;
        try {
            stream = trimPriceData(html);
            XmlPullParser parser = getParser(stream);
            parser.nextTag(); // @ div
            parser.next(); // @ span
            parser.nextTag(); // @ span
            parser.nextTag(); // @ br
            parser.nextTag(); // @ br
            parser.next(); // @ span
            parser.next(); // @ span
            stock.setPrice(readText(parser));
        } finally {
            StreamUtils.closeInputStream(stream);
        }
    }
}
