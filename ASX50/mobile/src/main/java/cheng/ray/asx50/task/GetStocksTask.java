package cheng.ray.asx50.task;


import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cheng.ray.asx50.model.ASX50CodeParser;
import cheng.ray.asx50.model.ASX50Parser;
import cheng.ray.asx50.model.ASX50StockParser;
import cheng.ray.asx50.model.Stock;
import cheng.ray.asx50.persistence.StockPriceDbHelper;
import cheng.ray.asx50.util.HttpUtils;

public class GetStocksTask extends AsyncTask<Void, Void, List<Stock>>{
    private static final String ASX_CODE_LIST_SOURCE = "https://www.asx50list.com/";
    private static final String STOCK_SOURCE_FORMAT = "http://www.reuters.com/finance/stocks/overview?symbol=%s.AX";
    private static final String DESKTOP_UA = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0";
    public interface ITaskOnPostExecuteCallback {
        void onPostExecute(List<Stock> result);
    }

    private ITaskOnPostExecuteCallback callback;
    private boolean refresh = false;
    private StockPriceDbHelper helper;
    public GetStocksTask(StockPriceDbHelper helper, ITaskOnPostExecuteCallback callback, boolean refresh) {
        this.helper = helper;
        this.callback = callback;
        this.refresh = refresh;
    }

    @Override
    protected List<Stock> doInBackground(Void... params) {
        Log.d(getClass().getName(), "isCancelled = " + isCancelled());
        if (refresh){
            List<Stock> stocks = getStocksFromWeb();
            // List<Stock> stocks = getStocksFromTest();
            if (stocks != null && stocks.size() > 0) {
                helper.truncate();
                helper.insertStocks(stocks);
            }
        }

        return helper.getStocks(null, null);

    }
    /*
    private List<Stock> getStocksFromTest(){
        List<Stock> stocks = new ArrayList<>();
        String[] codes = {"AGL","CBA","MQG"};
        for (String code:codes){
            Stock stock = new Stock(code);
            stock.setPrice(1.0f);
            stocks.add(stock);
        }
        return stocks;
    }*/

    private List<Stock> getStocksFromWeb(){
        String codeHtml = HttpUtils.downloadData(ASX_CODE_LIST_SOURCE, null);
        ASX50Parser codeParser = new ASX50CodeParser();
        List<Stock> stocks = new ArrayList<>();
        try {
            if (!TextUtils.isEmpty(codeHtml)) {
                List<String> codes = (List<String>) codeParser.parse(codeHtml);
                if (codes != null) {
                    ASX50StockParser stockParser = new ASX50StockParser();

                    for (String code : codes) {
                        String stockHtml = HttpUtils.downloadData(String.format(STOCK_SOURCE_FORMAT, code), DESKTOP_UA);
                        stockParser.setCode(code);
                        Stock stock = stockParser.parse(stockHtml);
                        if (stock != null) {
                            stocks.add(stock);
                        }
                    }
                }
            }
        } catch (XmlPullParserException | IOException e) {
            Log.e(getClass().getName(), "stocks parsing: ", e);
        }
        return stocks;
    }

    @Override
    protected void onPostExecute(List<Stock> result) {
        if (callback != null) {
            callback.onPostExecute(result);
        }
        helper = null;
    }
}
