package cheng.ray.asx50.util;

import android.text.TextUtils;

import org.apache.http.params.HttpConnectionParams;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class HttpUtils {
    public static String downloadData(String address, String userAgent) {
        HttpURLConnection conn = null;
        InputStream stream = null;
        String output = null;
        try {
            URL url = new URL(address);
            conn = (HttpURLConnection) url.openConnection();
            if (!TextUtils.isEmpty(userAgent)){
                conn.setRequestProperty("User-Agent", userAgent);
            }
            stream = conn.getInputStream();
            output = StreamUtils.streamToString(stream);
        } catch (IOException ignored) {
        } finally {
            if (conn != null){
                conn.disconnect();
            }
            StreamUtils.closeInputStream(stream);
        }

        return output;
    }
}
