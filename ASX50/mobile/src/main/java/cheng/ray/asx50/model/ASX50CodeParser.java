package cheng.ray.asx50.model;

import android.text.TextUtils;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cheng.ray.asx50.util.StreamUtils;

public class ASX50CodeParser extends ASX50Parser{
    private static final String START_HEADER = "ASX 50 List (";
    private static final String START_TBODY_TAG = "<tbody";

    private static final String END_TBODY_TAG = "</tbody>";

    private InputStream trimData(String html) {
        InputStream output = null;
        try {
            if (!TextUtils.isEmpty(html)){
                int start = html.indexOf(START_HEADER);
                start = html.indexOf(START_TBODY_TAG, start);
                int end = html.indexOf(END_TBODY_TAG, start);
                String trimmed = TextUtils.substring(html, start, end + END_TBODY_TAG.length()).replace("\n","").replace("&nbsp;","");
                // Log.d(getClass().getSimpleName(), "trimmed = " + trimmed);

                output = StreamUtils.stringToStream(trimmed);
            }
        } catch (IOException | IndexOutOfBoundsException ignored){}
        return output;
    }

    private boolean readValues(XmlPullParser parser, List<String> output) throws XmlPullParserException, IOException {
        String code = null;
        parser.nextTag();
        String currentName = parser.getName();
        if ("td".equals(currentName) && parser.getEventType() == XmlPullParser.START_TAG){
            code = readText(parser);
            output.add(code);
        }

        return !TextUtils.isEmpty(code);
    }

    @Override
    public List<String> parse(String html) throws XmlPullParserException, IOException {
        InputStream stream = null;
        List<String> output = null;
        try {
            stream = trimData(html);
            if (stream != null) {
                output = readData(getParser(stream));
            }
        } finally {
            StreamUtils.closeInputStream(stream);
        }
        return output;
    }

    private List<String> readData(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<String> output = new ArrayList<>();
        parser.require(XmlPullParser.START_TAG, null, REQUIRE_TAG);
        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            if ("tr".equals(parser.getName())){
                readValues(parser, output);
            }
        }
        return output;
    }

}



