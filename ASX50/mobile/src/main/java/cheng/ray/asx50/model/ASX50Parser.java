package cheng.ray.asx50.model;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

public abstract class ASX50Parser {
    public static final String REQUIRE_TAG = "tbody";

    public abstract Object parse(String html) throws XmlPullParserException, IOException;

    public ASX50Parser(){}

    /* protected void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }

        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }

    } */

    protected String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String output = null;
        if (parser.next() == XmlPullParser.TEXT) {
            output = parser.getText();
            parser.nextTag();
        }
        return output;
    }


    protected XmlPullParser getParser(InputStream stream) throws XmlPullParserException, IOException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(stream, null);
        parser.nextTag();
        return parser;
    }
}
