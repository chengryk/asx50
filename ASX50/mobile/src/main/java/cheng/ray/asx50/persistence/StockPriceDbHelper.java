package cheng.ray.asx50.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cheng.ray.asx50.model.Stock;

public class StockPriceDbHelper extends SQLiteOpenHelper{
    private static final String DB_NAME = "StockPrice";
    private static final int DB_VERSION = 1;
    private static final String TABLE_NAME = Stock.class.getSimpleName();

    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            + Stock.COLUMN_CODE + " TEXT PRIMARY KEY, "
            + Stock.COLUMN_PRICE + " REAL, "
            + Stock.COLUMN_PERATIO + " REAL, "
            + Stock.COLUMN_YIELD + " REAL, "
            + Stock.COLUMN_BETA + " REAL"
            + " )";

    public StockPriceDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public void truncate(){
        getReadableDatabase().delete(TABLE_NAME, null, null);
    }

    public void insertStocks(List<Stock> stocks){
        if (stocks != null && stocks.size() > 0) {
            SQLiteDatabase db = getReadableDatabase();
            db.beginTransaction();

            for (Stock stock:stocks) {
                ContentValues values = new ContentValues();
                values.put(Stock.COLUMN_CODE, stock.getCode());
                values.put(Stock.COLUMN_PRICE, stock.getPrice());
                values.put(Stock.COLUMN_YIELD, stock.getYield());
                values.put(Stock.COLUMN_BETA, stock.getBeta());
                values.put(Stock.COLUMN_PERATIO, stock.getPeRatio());

                long result = db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }

    public List<Stock> getStocks(List<String> codes, String orderBy){
        List<Stock> output = new ArrayList<Stock>();
        String selection = null;
        String[] selectionArg = null;
        if (codes != null && codes.size() == 0){
            return output;
        } else if (codes != null) {
            selection = "Stock.COLUMN_CODE in (?)";
            selectionArg = new String[]{"\"" + TextUtils.join("\",\"", codes) + "\""};
        }
        Log.d(getClass().getName(), "getStock selection = " + selectionArg);
        Cursor cursor = getReadableDatabase().query(TABLE_NAME, null, selection, selectionArg, null, null, orderBy);
        if (cursor != null && cursor.getCount() > 0){
            while (cursor.moveToNext()){
                Stock stock = new Stock(cursor.getString(cursor.getColumnIndex(Stock.COLUMN_CODE)));
                stock.setPrice(cursor.getFloat(cursor.getColumnIndex(Stock.COLUMN_PRICE)));
                stock.setPeRatio(cursor.getFloat(cursor.getColumnIndex(Stock.COLUMN_PERATIO)));
                stock.setYield(cursor.getFloat(cursor.getColumnIndex(Stock.COLUMN_YIELD)));
                stock.setBeta(cursor.getFloat(cursor.getColumnIndex(Stock.COLUMN_BETA)));
                output.add(stock);
            }
            cursor.close();
        }
        return output;
    }
}
