package cheng.ray.asx50.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cheng.ray.asx50.R;
import cheng.ray.asx50.model.Stock;

public class StockAdapter extends ArrayAdapter<Stock>{
    public StockAdapter(Context context, int resource) {
        super(context, resource);
    }

    public StockAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public StockAdapter(Context context, int resource, Stock[] objects) {
        super(context, resource, objects);
    }

    public StockAdapter(Context context, int resource, int textViewResourceId, Stock[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public StockAdapter(Context context, int resource, List<Stock> objects) {
        super(context, resource, objects);
    }

    public StockAdapter(Context context, int resource, int textViewResourceId, List<Stock> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (position >= 0) {
            Stock stock = getItem(position);
            if (stock != null) {
                if (view == null) {
                    view = LayoutInflater.from(getContext()).inflate(R.layout.entry_stock_price, parent, false);
                }
                final TextView tvCode = (TextView) view.findViewById(R.id.txt_code);
                tvCode.setText(stock.getCode());
                final TextView tvBeta = (TextView) view.findViewById(R.id.txt_beta);
                tvBeta.setText("" + stock.getBeta());
                final TextView tvPeRatio = (TextView) view.findViewById(R.id.txt_peratio);
                tvPeRatio.setText("" + stock.getPeRatio());
                final TextView tvPrice = (TextView) view.findViewById(R.id.txt_price);
                tvPrice.setText("" + stock.getPrice());
                final TextView tvYield = (TextView) view.findViewById(R.id.txt_yield);
                tvYield.setText("" + stock.getYield());
            }
        }
        return view;
    }
}
