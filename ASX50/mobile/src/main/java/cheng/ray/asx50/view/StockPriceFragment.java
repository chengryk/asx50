package cheng.ray.asx50.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import cheng.ray.asx50.R;
import cheng.ray.asx50.application.ASX50Application;
import cheng.ray.asx50.model.Stock;
import cheng.ray.asx50.task.GetStocksTask;

public class StockPriceFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener{
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListView mListView;
    private GetStocksTask mTask;

    private View headerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View v = inflater.inflate(R.layout.fragment_stock_price, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_priceyield);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mListView = (ListView) v.findViewById(R.id.list_priceyield);
        mListView.setOnScrollListener(this);
        mListView.setEmptyView(v.findViewById(R.id.scroll_price_empty));

        headerView = View.inflate(getContext(), R.layout.entry_stock_price, null);
        mListView.addHeaderView(headerView);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_stock_price, menu);
        // super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh && !mSwipeRefreshLayout.isRefreshing()) {
            startGetStockTask(true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSwipeRefreshLayout.setRefreshing(true);
        startGetStockTask(false);
    }

    @Override
    public void onPause() {
        if (mTask != null){
            mTask.cancel(true);
            mTask = null;
        }
        mSwipeRefreshLayout.setRefreshing(false);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        startGetStockTask(true);
    }

    private void startGetStockTask(Boolean refresh){
        mSwipeRefreshLayout.setRefreshing(true);
        mTask = new GetStocksTask(((ASX50Application)getActivity().getApplication()).getStockPriceDbHelper(), new OnPostExecuteCallback(), refresh);
        mTask.execute();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {}

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int topRowVerticalPosition = (view == null || view.getChildCount() == 0) ? 0 : view.getChildAt(0).getTop();
        mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
    }

    private class OnPostExecuteCallback implements GetStocksTask.ITaskOnPostExecuteCallback {
        @Override
        public void onPostExecute(List<Stock> result) {
            if (isResumed()){
                mListView.setAdapter(null);
                mListView.setAdapter(new StockAdapter(getContext(), R.layout.entry_stock_price, result));
                mListView.removeHeaderView(headerView);
                if (result != null && result.size() > 0){
                    mListView.addHeaderView(headerView);
                }
            }
            if (mSwipeRefreshLayout != null){
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }
}
