package cheng.ray.asx50.model;

import android.text.TextUtils;

public class Stock {
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_PERATIO = "peRatio";
    public static final String COLUMN_YIELD = "yield";
    public static final String COLUMN_BETA = "beta";

    private String code;
    private float price = 0.0f;
    private float peRatio = 0.0f; // Forward P/E
    private float yield = 0.0f; // Forward annual dividend yield
    private float beta = 0.0f; // Beta

    public Stock(String code){
        this.code = code;
    }

    public float getPrice() {
        return price;
    }

    public String getCode() {
        return code;
    }

    public float getPeRatio() {
        return peRatio;
    }

    public float getYield() {
        return yield;
    }

    public float getBeta() {return beta;}

    public void setPeRatio(float peRatio) {this.peRatio = peRatio;}

    public void setYield(float yield) {this.yield = yield;}

    public void setBeta(float beta) {this.beta = beta;}

    public void setPrice(float price){this.price = price;}

    public void setPeRatio(String peRatioString) {
        this.peRatio = parseFloat(peRatioString);
    }

    public void setYield(String yieldString) {
        yield = parseFloat(yieldString);
    }

    public void setBeta(String betaString) {
        beta = parseFloat(betaString);
    }

    public void setPrice(String priceString){
        price = parseFloat(priceString);
    }

    private float parseFloat(String floatString){
        float output = 0.0f;
        if (!TextUtils.isEmpty(floatString)) {
            try {
                output = Float.parseFloat(floatString.trim());
            } catch (Exception ignored) {
            }
        }
        return output;
    }

    public String toString(){
        return code + "," + price + "," + yield + "," + peRatio + "," + beta;
    }
}
